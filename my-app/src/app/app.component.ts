import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<h1>{{title}}</h1><h2>{{hero.name}} details!</h2>
  <app-click></app-click>
  `,
  styleUrls: ['./app.component.css']
  
})
export class AppComponent {
  title = 'Tour of heroes';
  hero = 'windstorm';
}
export class Hero {
  id:number;
  name:string;
}
